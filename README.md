# gtrans

A wrapper for `trans` that uses `xsel` to fetch current selection and output its translation into a GUI window.

Relies on [translate-shell project](https://github.com/soimort/translate-shell) for translation. Refer to man(1) trans for details.

Chances are high the **translate-shell** package in your distro is too old and may not work at all. The best way to get the latest trans version is to download [git.io/trans](https://git.io/trans), `chmod +x trans` and put it somewhere in your $PATH, e.g. `~/.local/bin/`. Also make sure to have the dependencies of **trans** itself installed. **gawk** and **curl** are essentional ones.

Relies on **xsel** to fetch selected text. Install it with you distro package manager, e.g. `apt install xsel`.

Install **zenity**, **qarma**, **gxmessage** or **kdialog**. Chances are high you have either **zenity** or **kdialog** installed. `which {zenity,qarma,gxmessage,kdialog}` will tell you which ones are available.

Once having the above, download [gtrans script](https://gitlab.com/skidnik/gtrans/raw/master/gtrans), `chmod +x gtrans`  and put it somewhere in your $PATH.

Bind **gtrans** with whatever options you see fit to some global key combination. Example commands:

    gtrans zenity -- :en

will use zenity to create a popup window with translation, will autodetect source language and translate it into english.

    gtrans -- en:ru

will autodetect the utility to create a popup window, assume source language is english and translate into russian.

Refer to `gtrans --help` for more info.
